/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  body: string
  franchiseName: string
  replyTo: string
  type: string
  courseName?: string
}

export const sampleData: IModel[] = [
  {
    body: "Hi, I'd like to know more about this course.",
    franchiseName: "BEON1X",
    replyTo: "nathan@example.com",
    type: "Course Enrolement",
    courseName: "Music Production Masterclass",
  },
  {
    body: "Hello, do you have any instructors who know about music?",
    franchiseName: "BEON1X",
    replyTo: "nathan@example.com",
    type: "General Enquiry",
  },
]
