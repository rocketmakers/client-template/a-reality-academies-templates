/**
 * Specify required object
 * TODO - THIS EMAIL IS DEPRECATED AND SHOULD BE REMOVED ONCE THIS MESSAGE IS ON THE PRODUCTION BRANCH
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  username: string
  returnUrl: string
}

export const sampleData: IModel[] = [
  {
    username: "test@rocketmakers.com",
    returnUrl: "https://www.rocketmakers.com",
  },
  {
    username: "test2@rocketmakers.com",
    returnUrl: "https://www.rocketmakers.com",
  },
]
