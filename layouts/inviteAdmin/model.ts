/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  returnUrl: string
  firstName: string
  franchiseName: string
}

export const sampleData: IModel[] = [
  {
    firstName: "Jim",
    returnUrl: "https://www.rocketmakers.com",
    franchiseName: "Test Academy",
  },
  {
    firstName: "Jane",
    returnUrl: "https://www.rocketmakers.com",
    franchiseName: "Test Academy 2",
  },
]
