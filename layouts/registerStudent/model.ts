/**
 * Specify required object
 * TODO - THIS EMAIL IS DEPRECATED AND SHOULD BE REMOVED ONCE THIS MESSAGE IS ON THE PRODUCTION BRANCH
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  returnUrl: string
  firstName: string
  franchiseName: string
  courseName?: string
}

export const sampleData: IModel[] = [
  {
    returnUrl: "https://www.rocketmakers.com",
    firstName: "Jim",
    franchiseName: "Test Academy",
    courseName: "Course Number 1",
  },
  {
    returnUrl: "https://www.rocketmakers.com",
    firstName: "Jane",
    franchiseName: "Test Academy 2",
  },
]
