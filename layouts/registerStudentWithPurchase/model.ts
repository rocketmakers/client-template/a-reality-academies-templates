/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  returnUrl: string
  franchiseName: string
  courseName: string
}

export const sampleData: IModel[] = [
  {
    returnUrl: "https://www.rocketmakers.com",
    franchiseName: "Test Academy",
    courseName: "Course Number 1",
  },
  {
    returnUrl: "https://www.rocketmakers.com",
    franchiseName: "Test Academy 2",
    courseName: "Course Number 2",
  },
]
