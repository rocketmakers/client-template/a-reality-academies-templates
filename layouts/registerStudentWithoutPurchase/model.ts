/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  email: string
  returnUrl: string
  franchiseName: string
}

export const sampleData: IModel[] = [
  {
    email: "test@user.com",
    returnUrl: "https://www.rocketmakers.com",
    franchiseName: "Test Academy",
  },
  {
    email: "test2@user.com",
    returnUrl: "https://www.rocketmakers.com",
    franchiseName: "Test Academy 2",
  },
]
