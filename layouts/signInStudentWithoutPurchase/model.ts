/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  returnUrl: string
  firstName: string
  email: string
}

export const sampleData: IModel[] = [
  {
    returnUrl: "https://www.rocketmakers.com",
    firstName: "Jim",
    email: "user@test.com",
  },
  {
    returnUrl: "https://www.rocketmakers.com",
    firstName: "Jane",
    email: "user2@test.com",
  },
]
