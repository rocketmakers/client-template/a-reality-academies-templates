/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  body: string
  franchiseName: string
  replyTo: string
  name: string
}

export const sampleData: IModel[] = [
  {
    body: "My headset is burning my head, please help",
    franchiseName: "Hannes Bieger Academy",
    replyTo: "joe@example.com",
    name: "Joe Lewsey",
  },
  {
    body: "How on earth do I use this thing!?",
    franchiseName: "Oliver Leib Academy",
    replyTo: "joe@example.com",
    name: "Joe Lewsey",
  },
]
